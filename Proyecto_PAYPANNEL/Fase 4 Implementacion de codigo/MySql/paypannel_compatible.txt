CREATE TABLE  Roles  (
   idRoles  INT NOT NULL,
   Rol  VARCHAR(50) NOT NULL,
  PRIMARY KEY ( idRoles ))
ENGINE = InnoDB;

CREATE TABLE Usuario  (
   idUsuario  INT NOT NULL AUTO_INCREMENT,
   Tipo_documento  CHAR(2) NOT NULL,
   Documento  BIGINT(20) NOT NULL,
   Nombre  VARCHAR(50) BINARY NOT NULL,
   Apellido  VARCHAR(50) BINARY NOT NULL,
   Telefono  BIGINT(12) NOT NULL,
   Correo  VARCHAR(40) NOT NULL,
   Anotaciones  VARCHAR(300) NOT NULL DEFAULT  Pendiente ,
   Roles_idRoles  INT NOT NULL,
  PRIMARY KEY ( idUsuario ),
  INDEX  fk_Usuario_Roles1_idx  ( Roles_idRoles  ASC),
  CONSTRAINT  fk_Usuario_Roles1 
    FOREIGN KEY ( Roles_idRoles )
    REFERENCES Roles  ( idRoles )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE Permiso  (
   idPermisos  INT NOT NULL,
   Estadoper  INT(1) NOT NULL DEFAULT 0,
   Contrasena  VARCHAR(50) NOT NULL,
   Fecha  DATE NOT NULL,
   Usuario_idUsuario  INT NOT NULL,
  PRIMARY KEY ( idPermisos ),
  INDEX  fk_Permiso_Usuario1_idx  ( Usuario_idUsuario  ASC),
  CONSTRAINT  fk_Permiso_Usuario1 
    FOREIGN KEY ( Usuario_idUsuario )
    REFERENCES Usuario  ( idUsuario )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

CREATE TABLE  Lora  (
   idLora  INT NOT NULL,
   Ubicacion  VARCHAR(45) NOT NULL,
   Ciudad  VARCHAR(45) NULL,
   Fechalora  DATE NULL,
   Estado  INT(1) NULL,
  PRIMARY KEY ( idLora ))
ENGINE = InnoDB;


CREATE TABLE Componente  (
   idComponente  INT NOT NULL AUTO_INCREMENT,
   DevEUI  VARCHAR(45) NOT NULL,
   Energia_requerida  VARCHAR(45) NOT NULL,
   Estadocom  INT(1) NOT NULL,
   Created_by  INT(20) NULL,
   Tipo_componente  VARCHAR(45) NOT NULL,
   Lora_idLora  INT NOT NULL,
   Usuario_idUsuario  INT NOT NULL,
  PRIMARY KEY ( idComponente ),
  INDEX  fk_Componente_Lora1_idx  ( Lora_idLora  ASC),
  INDEX  fk_Componente_Usuario1_idx  ( Usuario_idUsuario  ASC),
  CONSTRAINT  fk_Componente_Lora1 
    FOREIGN KEY ( Lora_idLora )
    REFERENCES Lora  ( idLora )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT  fk_Componente_Usuario1 
    FOREIGN KEY ( Usuario_idUsuario )
    REFERENCES Usuario  ( idUsuario )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


CREATE TABLE  Energia_recolectada  (
   idrecolectada  INT NOT NULL AUTO_INCREMENT,
   Registrada  INT(10) NULL,
   Fecha_com  DATETIME NULL,
   Componente_idComponente  INT NOT NULL,
  PRIMARY KEY ( idrecolectada ),
  INDEX  fk_Energia_recolectada_Componente1_idx  ( Componente_idComponente  ASC) ,
  CONSTRAINT  fk_Energia_recolectada_Componente1 
    FOREIGN KEY ( Componente_idComponente )
    REFERENCES Componente  ( idComponente )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

